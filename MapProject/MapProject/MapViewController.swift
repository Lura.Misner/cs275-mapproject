//
//  MapViewController.swift
//  MapProject
//
//  Created by user203105 on 11/21/21.
//

import UIKit
import MapKit

class MapViewController: UIViewController {

    var mapView: MKMapView!
    
    override func loadView() {
        // Create a map view
        mapView = MKMapView()
        
        // Set it as the view of this view controller
        view = mapView
        
        let segmentedControl = UISegmentedControl(items: ["Standard", "Hybrid", "Satellite"])
        segmentedControl.backgroundColor = UIColor.systemBackground
        segmentedControl.selectedSegmentIndex = 0
        
        segmentedControl.addTarget(self, action: #selector(mapTypeChanged(_:)), for: .valueChanged)
        
        segmentedControl.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(segmentedControl)
        
        let topConstraint = segmentedControl.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 8)
        let margins = view.layoutMarginsGuide
        let leadingConstraint = segmentedControl.leadingAnchor.constraint(equalTo: margins.leadingAnchor)
        let trailingConstraint = segmentedControl.trailingAnchor.constraint(equalTo: margins.trailingAnchor)
        
        topConstraint.isActive = true
        leadingConstraint.isActive = true
        trailingConstraint.isActive = true
        
        // Label and Switch
        let label = UILabel()
        label.text = "Points of Interest"
        label.textColor = UIColor.systemRed
        label.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(label)
        
        label.leadingAnchor.constraint(equalTo: margins.leadingAnchor).isActive = true
        label.topAnchor.constraint(equalTo: segmentedControl.bottomAnchor, constant: 8).isActive = true
        
        let poiSwitch = UISwitch()
        poiSwitch.setOn(true, animated: true)
        poiSwitch.addTarget(self, action: #selector(poiDisplay(_:)), for: .valueChanged)
        poiSwitch.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(poiSwitch)
        
        poiSwitch.topAnchor.constraint(equalTo: segmentedControl.bottomAnchor, constant: 8).isActive = true
        poiSwitch.leadingAnchor.constraint(equalTo: label.trailingAnchor, constant: 12).isActive = true
        
        // Find Me Button
        let fmButton = UIButton()
        fmButton.backgroundColor = UIColor.white
        fmButton.setTitle("Find Me", for: .normal)
        fmButton.setTitleColor(UIColor.black, for: .normal)
        fmButton.translatesAutoresizingMaskIntoConstraints = false
        fmButton.layer.borderWidth = 1
        fmButton.layer.cornerRadius = 2
        fmButton.addTarget(self, action: #selector(pressed), for: .touchDown)
        view.addSubview(fmButton)
        
        fmButton.leadingAnchor.constraint(equalTo: margins.leadingAnchor).isActive = true
        fmButton.topAnchor.constraint(equalTo: label.bottomAnchor, constant: 8).isActive = true
        fmButton.widthAnchor.constraint(equalToConstant: fmButton.titleLabel!.intrinsicContentSize.width + 2.0 * 3 ).isActive = true
    }
    
    @objc func pressed() {
        // Zooms in on New York
        let span = MKCoordinateSpan(latitudeDelta: 0.1, longitudeDelta:  0.1)
        let location = CLLocationCoordinate2DMake(40.7, -74)
        let region = MKCoordinateRegion(center: location, span: span)
        mapView.setRegion(region, animated: true)
    }
    
    @objc func poiDisplay(_ poi: UISwitch){
        if (poi.isOn == false) {
            // Turn off
            mapView.pointOfInterestFilter = MKPointOfInterestFilter(including: [])
            poi.setOn(false, animated: true)
        } else {
            // Turn on
            mapView.pointOfInterestFilter = MKPointOfInterestFilter(excluding: [])
            poi.setOn(true, animated: true)
        }
    }
    
    @objc func mapTypeChanged(_ segControl: UISegmentedControl) {
        switch segControl.selectedSegmentIndex {
        case 0:
            mapView.mapType = .standard
        case 1:
            mapView.mapType = .hybrid
        case 2:
            mapView.mapType = .satellite
        default:
            break
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        print("Map controller loaded its view")
    }
}
